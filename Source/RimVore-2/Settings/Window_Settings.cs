﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RimVore2
{
    public class Window_Settings : Window
    {
        private static int currentTabIndex;
        private static List<TabRecord> tabs;
        private SettingsTab CurrentTab => tabs.Count > currentTabIndex ? (SettingsTab)tabs[currentTabIndex] : null;

        private const float TabHeight = 32f;  // I think base game has 31 as the tab height, hard to tell with the compiler optimized constants 
        private Color SelectedColor => Color.green;
        private static TabRecord selectedTab;

        public Window_Settings()
        {
            doCloseX = true;
            absorbInputAroundWindow = true;
            focusWhenOpened = true;
            forcePause = true;
            draggable = true;
            resizeable = true;
            onlyOneOfTypeAllowed = true;
            UIUtility.OverwriteResizer(this);
        }

        public override void PreClose()
        {
            base.PreClose();
            if(RV2Mod.Mod != null)
            {
                RV2Mod.Mod.WriteSettings();
            }
            VoreInteractionManager.ClearCachedInteractions();
        }

        public override void PostClose()
        {
            base.PostClose();
            RV2Mod.Settings.rules.RecacheAllDesignations();
        }

        public override Vector2 InitialSize
        {
            get
            {
                return new Vector2(UI.screenWidth * 0.4f, UI.screenHeight * 0.6f);
            }
        }

        public override void DoWindowContents(Rect inRect)
        {
            inRect = new Rect(
                inRect.x,
                inRect.y,
                inRect.width,
                inRect.height);
            InitializeTabs();
            Listing_Standard list = new Listing_Standard()
            {
                maxOneColumn = true
            };
            list.Begin(inRect);

            int rows = (int)Math.Ceiling((float)tabs.Count / 7);
            Rect drawRect = new Rect(
                inRect.x,
                inRect.y + rows * TabHeight,
                inRect.width,
                inRect.height - rows * TabHeight);
            // okay, the tab drawer is fucking weird, you give it a Rectangle and it will do its best to draw ABOVE the rectangle you gave it rather than inside the rectangle
            TabRecord record = TabDrawer.DrawTabs(drawRect, tabs, rows);
#if !v1_2
            if(record != null)
            {
                if(selectedTab == null)
                {
                    selectedTab = record;
                    record.labelColor = SelectedColor;
                }
                else if(selectedTab != record)
                {
                    selectedTab.labelColor = record.labelColor;
                    selectedTab = record;
                    record.labelColor = SelectedColor;
                }
            }
#endif
            // lower the box a bit because the tab drawer leaves no space
            drawRect.y += 3;
            drawRect.height -= 3;
            CurrentTab?.FillRect(drawRect);

            list.End();
        }
        private static void InitializeTabs()
        {
            if(tabs == null)
            {
                tabs = new List<TabRecord>();
            }
            if(Prefs.DevMode)
            {
                AddTab(new SettingsTab_Debug("RV2_Settings_TabNames_Debug".Translate(), null, null));
            }
            AddTab(new SettingsTab_Features("RV2_Settings_TabNames_Features".Translate(), null, null));
            AddTab(new SettingsTab_FineTuning("RV2_Settings_TabNames_FineTuning".Translate(), null, null));
            AddTab(new SettingsTab_Sounds("RV2_Settings_TabNames_Sounds".Translate(), null, null));
            AddTab(new SettingsTab_Cheats("RV2_Settings_TabNames_Cheats".Translate(), null, null));
            AddTab(new SettingsTab_Rules("RV2_Settings_TabNames_Rules".Translate(), null, null));
            AddTab(new SettingsTab_Quirks("RV2_Settings_TabNames_Quirks".Translate(), null, null));
            AddTab(new SettingsTab_Combat("RV2_Settings_TabNames_Combat".Translate(), null, null));
#if !v1_2
            if(ModLister.IdeologyInstalled)
            {
                AddTab(new SettingsTab_Ideology("RV2_Settings_TabNames_Ideology".Translate(), null, null));
            }
#endif
        }
        static Func<SettingsTab, Action> clickedAction = (SettingsTab tab) => () => currentTabIndex = tabs.IndexOf(tab);
        static Func<SettingsTab, Func<bool>> selectedGetter = (SettingsTab tab) => () => currentTabIndex == tabs.IndexOf(tab);
        // the RecordTab constructor is bad, so we call it with NULL clickedAction and selectedGetter in order to add them afterwards with reference to the created tab
        public static void AddTab(SettingsTab tab)
        {
            // each tab type can only exist once
            if(tabs.Any(t => t.GetType() == tab.GetType()))
            {
                // Log.Message("tabs already contains tab of type " + tab.GetType().ToString());
                return;
            }
            tab.clickedAction = clickedAction(tab);
            tab.selectedGetter = selectedGetter(tab);
            tabs.Add(tab);
        }
    }
}
